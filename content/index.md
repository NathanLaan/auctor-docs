---
layout: easion
title: Auctor Docs
permalink: /
activeMenu: menu-home
activePage: menu-home
---

# Auctor Documentation

Auctor (pronounced awkTOHR) is a simple Node.js based static site generator that uses Markdown or EJS JavaScript templates for content and EJS JavaScript templates for layout templates.

## Features

- Very simple.
- Supports Markdown or EJS JavaScript for page templates.
- Supports EJS includes.

## Links

- [NPM Page for Auctor Package](https://www.npmjs.com/package/auctor)
- [Auctor Source Code](https://github.com/nathanlaan/auctor)
- [Auctor Documentation Source Code](https://gitlab.com/NathanLaan/auctor-docs)
