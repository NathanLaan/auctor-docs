/**
 * Build script for the Auctor Documentation site.
 */
const auctor = require('auctor');
const yml = require('yml');
const cfg = yml.load('./_build/config.yml', process.env.NODE_ENV);
cfg.versionNumber = process.env.npm_package_version;
auctor(cfg);
