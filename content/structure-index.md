---
layout: easion
title: Auctor Docs - Site Structure
permalink: /structure
activeMenu: menu-structure
activePage: menu-structure
---

# Auctor Site Structure

TODO: Introduction.

TODO: Convention over configuration.

## Sample Site Structure

- /
  - /_build
    - build.js
    - config.yml
  - /_layout
    - template.ejs
  - /assets
    - /css
    - /js
    - /img
  - /content
    - index.md
    - about.ejs
    - /portfolio
      - index.md
      - work.md
  - package.json
