---
layout: easion
title: Project Setup
permalink: /guide
activeMenu: menu-guide
activePage: menu-guide
---

# Auctor Setup Guide

TODO: Introduction.

## 1. Install Auctor

Open terminal, navigate to the directory for your project, and run `npm i auctor`. This will install the Auctor package and   create your initial `package.json` file.

## 2. Update `package.json` File

The `package.json` file contains the list of dependencies, as well as the configuration for Auctor. The Auctor package has no hard dependencies, but in the code and configuration samples, we are using `yml` to load configuration settings, `cross-env` to build the Auctor output, and `serve` to serve the output files locally for testing.

```json
{
  "name": "project-name",
  "version": "1.3.0",
  "description": "project-description",
  "scripts": {
    "build": "cross-env NODE_ENV=dev node ./_build/build",
    "build:prod": "cross-env NODE_ENV=prod node ./_build/build",
    "serve": "serve ./test"
  },
  "keywords": [project-keywords],
  "author": "project-author",
  "license": "MIT",
  "dependencies": {
    "auctor": "^1.3.0",
    "cross-env": "^5.1.4",
    "serve": "^14.2.3",
    "yml": "^1.0.0"
  }
}
```

## 3. `_build` Folder

TODO: Show basic folder structure.

## 4. `_layout` Folder

TODO: Show sample folder structure.

## 5. `assets` Folder

TODO: Show sample folder structure.

## 6. `content` Folder

TODO: Show sample folder structure.
